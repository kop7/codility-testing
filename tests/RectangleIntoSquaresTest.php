<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class RectangleIntoSquaresTest extends TestCase
{

  /**
   * https://www.codewars.com/kata/55466989aeecab5aac00003e/train/php
   * @dataProvider num
   */

  public function testectangleIntoSquares($array, $expected)
  {
      $alphabet = new \RectangleIntoSquares();

      $result = $alphabet->rectang($array);

      self::assertEquals($result, $expected);
  }

  public function num() {
    return
        [
      [[5,3], [3, 2, 1, 1]],
      [[3,5], [3, 2, 1, 1]],
      [[20,14], [14, 6, 6, 2, 2, 2]],

    ];
  }

}