<?php

namespace Tests;

class PassingCarsTest extends \PHPUnit_Framework_TestCase
{
     /**
     * @dataProvider provideCasesForPassingCars
     */
    public function testPassingCarPairs($cars, $expected){
      
        $passingCars = new \PassingCars();

        $result = $passingCars->solve($cars);

        self::assertEquals($expected, $result);
    }

    public function provideCasesForPassingCars(){
        return [
           [ [0 , 1, 0, 1, 1], 5],
           [ [0, 1, 0, 1, 1, 0, 1], 8],
           [[0, 1], 1],
           [[0, 0], 0],
           [[1, 1, 1], 0],
           [[0, 0, 1], 2]
        ];
    }

}