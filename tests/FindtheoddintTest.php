<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class FindtheoddintTest extends TestCase
{

  /**
   * Given an array, find the int that appears an odd number of times.
   * There will always be only one integer that appears an odd number of times.
   *
   * @dataProvider num
   */

  public function testFindtheoddint($array, $expected)
  {
      $odd = new \Findtheoddint();

      $result = $odd->findIt2($array);

      self::assertEquals($result, $expected);
  }

  public function num() {
    return
        [
           [[20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5], 5],
           [[1,1,2,-2,5,2,4,4,-1,-2,5], -1],
           [[20,1,1,2,2,3,3,5,5,4,20,4,5], 5],
           [[10], 10],
           [[1,1,1,1,1,1,10,1,1,1,1], 10],
        ];
  }

}