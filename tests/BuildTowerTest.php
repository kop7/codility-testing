<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class BuildTowerTest extends TestCase
{

  /**
  Build Tower by the following given argument:
  number of floors (integer and always greater than 0).


   * @dataProvider string
   */

  public function testBuildTower($int, $expected)
  {
      $keb = new \BuildTower();

      $result = $keb->build($int);

      self::assertEquals($result, $expected);
  }

  public function string() {
    return
        [
      [3, ['  *  ',
           ' *** ',
           '*****']],
    ];
  }

}