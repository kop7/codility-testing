<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class AlphabetPositionTest extends TestCase
{

  /**
   * Welcome.
   *
   *In this kata you are required to, given a string, replace every letter with its position in the alphabet.
   *
   *If anything in the text isn't a letter, ignore it and don't return it.
   *
   *a being 1, b being 2, etc.
   *
   *As an example:
   *
   *
   * @dataProvider sentence
   */

  public function testAlphabetPosition($string, $expected)
  {
      $alphabet = new \ReplaceWithAlphabetPosition();

      $result = $alphabet->alphabetPosition2($string);


      self::assertEquals($result, $expected);
  }

  public function sentence() {
    return
        [
      ['The narwhal bacons at midnight.', '20 8 5 14 1 18 23 8 1 12 2 1 3 15 14 19 1 20 13 9 4 14 9 7 8 20']

    ];
  }

}