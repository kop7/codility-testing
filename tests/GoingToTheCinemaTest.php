<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class GoingToTheCinema extends TestCase
{

  /**
   * My friend John likes to go to the cinema. He can choose between system A and system B.
   *
   * System A : buy a ticket (15 dollars) every time
    System B : buy a card (500 dollars) and every time
    buy a ticket the price of which is 0.90 times the price he paid for the previous one.
   *
   * #Example: If John goes to the cinema 3 times:
   * System A : 15 * 3 = 45
        System B : 500 + 15 * 0.90 + (15 * 0.90) * 0.90 + (15 * 0.90 * 0.90) * 0.90 ( = 536.5849999999999, no rounding for each ticket)
   *
   * John wants to know how many times he must go to the cinema so that the final result of System B, when rounded up to the next dollar, will be cheaper than System A.

    The function movie has 3 parameters: card (price of the card), ticket (normal price of a ticket), perc (fraction of what he paid for the previous ticket) and returns the first n such that
   *
   * @dataProvider numbers
   */

  public function testGoingToTheCinema($num, $expected)
  {
      $cinema = new \GoingToTheCinema();

      $result = $cinema->cinema($num);


      self::assertEquals($result, $expected);
  }

  public function numbers() {
    return
        [
      [[500, 15, 0.9], 43],
      [[100, 10, 0.95], 24],

    ];
  }

}