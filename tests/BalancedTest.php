<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class BalancedTest extends TestCase
{

  /**
  Description:

  Each exclamation mark weight is 2; Each question mark weight is 3. Put two string left and right to the balance, Are they balanced?

  If the left side is more heavy, return "Left"; If the right side is more heavy, return "Right"; If they are balanced, return "Balance".
  Examples

  balance("!!","??") == "Right"
  balance("!??","?!!") == "Left"
  balance("!?!!","?!?") == "Left"
  balance("!!???!????","??!!?!!!!!!!") == "Balance"

   * @dataProvider string
   */

  public function testBalancedTest($str1, $str2, $expected)
  {
      $keb = new \Balanced();

      $result = $keb->balance($str1, $str2);

      self::assertEquals($result, $expected);
  }

  public function string() {
    return
        [
      [['!!'],['??'], 'Right'],
      [['!??'],['?!!'], 'Left'],
      [['!?!!'],['?!?'], 'Left'],
      [['!!???!????'],['??!!?!!!!!!!'], 'Balance'],

    ];
  }

}