<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class FindTwinsTest extends TestCase
{

  /**
   * FindTwins in array
   *
   * @dataProvider num
   */

  public function testFindTwins($array, $expected)
  {
      $twin = new \FindTwins();

      $result = $twin->elimination($array);


      self::assertEquals($result, $expected);
  }

  public function num() {
    return
        [
        [ [25,2,3,4,5,6,25], 25],
        [ [23,34,423,5,45,5,632,123], 5],
        [ [1,2,3,4,5,6,6], 6],
        [ [1,2,3,4,5,6,6,6,4,4545], 4],
    ];
  }

}