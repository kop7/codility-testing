<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

use PHPUnit\Framework\TestCase;

class NestingTest extends TestCase
{

  /**
   *
   * @dataProvider pNumbers
   */
  public function testNesting($string, $expected)
  {
    $nest = new \NestingCheck();

    $result = $nest->nesting($string);

    self::assertEquals($result, $expected);
  }

  public function pNumbers() {
    return[
      ['((()))',
        1],
      ['',
        1],
      ['((())',
        0],
      ['()()())',
        0],
    ];
  }

}