<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class ShortestWord extends TestCase
{

  /**
   * Simple, given a string of words, return the length of the shortest word(s).

  String will never be empty and you do not need to account for different data types.
   *
   * @dataProvider word
   */

  public function testShortestWord($word, $expected)
  {
      $century = new \ShortestWord();

      $result = $century->shortest($word);


      self::assertEquals($result, $expected);
  }

  public function word() {
    return
        [
      ['bitcoin take over the world maybe who knows perhaps', 3],
      ['turns out random test cases are easier than writing out basic ones', 3],
      ['turns out random test cases are easier than writing out basic ones a', 1]
    ];
  }

}