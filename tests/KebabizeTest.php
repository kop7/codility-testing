<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class KebabizeTest extends TestCase
{

  /**
    *Modify the kebabize function so that it converts a camel case string into a kebab case.
    *kebabize('camelsHaveThreeHumps') // camels-have-three-humps
    *kebabize('camelsHave3Humps') // camels-have-humps
   *
   * @dataProvider string
   */

  public function testDecipherThis($string, $expected)
  {
      $keb = new \Kebabize();

      $result = $keb->kebabizeThis($string);

      self::assertEquals($result, $expected);
  }

  public function string() {
    return
        [
      ['myCamelCasedString', 'my-camel-cased-string'],
      ['myCamelHas3Humps', 'my-camel-has-humps'],
      ['2FJEo', 'f-j-eo'],
    ];
  }

}