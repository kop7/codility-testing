<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class CamelCaseMethodTest extends TestCase
{

  /**
  Write simple .camelCase method (camel_case function in PHP, CamelCase in C# or camelCase in Java) for strings. All words must have their first letter capitalized without spaces.

  For instance:

  camel_case("hello case"); // => "HelloCase"
  camel_case("camel case word"); // => "CamelCaseWord"


   * @dataProvider string
   */

  public function testCamelCase($string, $expected)
  {
      $keb = new \CamelCaseMethod();

      $result = $keb->camel_case($string);

      self::assertEquals($result, $expected);
  }

  public function string() {
    return
        [
      //['test case', 'TestCase'],
      ['hello case', 'HelloCase'],
      //['camel case method', 'CamelCaseMethod'],


    ];
  }

}