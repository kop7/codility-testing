<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class GettheMiddleCharacterTest extends TestCase
{

  /**
   * You are going to be given a word. Your job is to return the middle character of the word. If the word's length is odd, return the middle character.
   * If the word's length is even, return the middle 2 characters.
   *
   * @dataProvider string
   */

  public function testGettheMiddleCharacter($string, $expected)
  {
      $mid = new \GettheMiddleCharacter();

      $result = $mid->middleCharacter($string);

      self::assertEquals($result, $expected);
  }

  public function string() {
    return
        [
      ['otoRino', 'R'],
            ['maRko', 'R'],
      ['marGGans', 'GG'],
      ['a', 'a'],
      ['testing', 't'],
      ['miDDle', 'DD'],



    ];
  }

}