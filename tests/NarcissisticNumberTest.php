<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class NarcissisticNumberTest extends TestCase
{

  /**
   * A Narcissistic Number is a number which is the sum of its own digits, each raised to the power of the number of digits.
   *
   *
   * @dataProvider num
   */

  public function testNarcissisticNumber($num, $expected)
  {
      $alphabet = new \NarcissisticNumber();

      $result = $alphabet->narcissistic($num);


      self::assertEquals($result, $expected);
  }

  public function num() {
    return
        [
      [ 153, 153],
    ];
  }

}