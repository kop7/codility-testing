<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 08.02.18.
 * Time: 09:53
 */

namespace Tests;

use PHPUnit\Framework\TestCase;

class PositiveIntegerTest extends TestCase
{

  /**
   * @dataProvider provideNumbers
   */
  public function testPositiveInteger($integers, $expected)
  {
    $pos = new \PositiveInteger();

    $result = $pos->posInteger($integers);

    self::assertEquals($expected, $result);
  }


  public function provideNumbers() {
    return[
      [[1,3,5,4,1,2],
        6],
      [[1,2,3],
        4],
      [[-1,-3],
        1],
      [[-2,-5,1,2],
        3],
      [[-2,5,-5,1,2],
        3],
      [[-1],
        1],
      [[2],
        3],
    ];
  }


}