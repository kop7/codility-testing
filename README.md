# Codility Testing

### How to setup and run tests

 - Clone this repository 
    
        git clone git@bitbucket.org:kop7/codility-testing.git

 - Go to codility folder
    
        cd codility-testing

 - Download composer.phar <https://getcomposer.org/download/>
       
        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        php -r "if (hash_file('sha384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
        php composer-setup.php
        php -r "unlink('composer-setup.php');"
    
 - Update Composer
       
        php composer.phar update
    
 - Run all test

        ./vendor/bin/phpunit tests
    
## Codility Task
    
- #### Replace With Alphabet Position
  
    Description:
    
        In this kata you are required to, given a string, replace every letter with its position in the alphabet.
        If anything in the text isn't a letter, ignore it and don't return it.
    
        "a" = 1, "b" = 2, etc.
        
        Example:
    
        alphabet_position("The sunset sets at twelve o' clock.")
        Should return "20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11" (as a string)

    Test task:
  
        ./vendor/bin/phpunit tests/AlphabetPositionTest.php
  
- #### Exclamation marks series #17: Put the exclamation marks and question marks to the balance, Are they balanced?
      
    Description
      
        Each exclamation mark weight is 2; Each question mark weight is 3. Put two string left and right to the balance, Are they balanced?
          
        If the left side is more heavy, return "Left"; If the right side is more heavy, return "Right"; If they are balanced, return "Balance".
           
        Examples:
          
        balance("!!","??") == "Right"
        balance("!??","?!!") == "Left"
        balance("!?!!","?!?") == "Left"
        balance("!!???!????","??!!?!!!!!!!") == "Balance"
            
    Test task:
    
        ./vendor/bin/phpunit tests/BalancedTest.php
          
- #### Build Tower
    
    Description:
      
        Build Tower by the following given argument:
        number of floors (integer and always greater than 0).
          
        Tower block is represented as *
          
              Python: return a list;
              JavaScript: returns an Array;
              C#: returns a string[];
              PHP: returns an array;
              C++: returns a vector<string>;
              Haskell: returns a [String];
              Ruby: returns an Array;
          
        Have fun!
          
        for example, a tower of 3 floors looks like below
          
        [
          '  *  ', 
          ' *** ', 
          '*****'
        ]
          
        and a tower of 6 floors looks like below
          
        [
          '     *     ', 
          '    ***    ', 
          '   *****   ', 
          '  *******  ', 
          ' ********* ', 
          '***********'
        ]

    Test task:
    
        ./vendor/bin/phpunit tests/BuildTowerTest.php
        
- #### Camel Case Method
    
    Description:
    
        Write simple .camelCase method (camel_case function in PHP, CamelCase in C# or camelCase in Java) for strings. All words must have their first letter capitalized without spaces.
        
        For instance:
        
        camel_case("hello case"); // => "HelloCase"
        camel_case("camel case word"); // => "CamelCaseWord"

    Test task:
  
        ./vendor/bin/phpunit tests/CamelCaseMethodTest.php
        
- #### Find the odd integer
    
    Description:
    
        Given an array, find the int that appears an odd number of times.
        There will always be only one integer that appears an odd number of times.

    Test task:
  
        ./vendor/bin/phpunit tests/FindtheoddintTest.php  
             
- #### Find Twins
    
    Description:
     
        FindTwins in array
         
    Test task:
   
        ./vendor/bin/phpunit tests/FindTwinsTest.php
      
- #### Get the Middle Character
     
    Description:
      
        You are going to be given a word. Your job is to return the middle character of the word. If the word's length is odd, return the middle character.
        If the word's length is even, return the middle 2 characters.
          
    Test task:
    
        ./vendor/bin/phpunit tests/GettheMiddleCharacterTest.php 
                
- #### Going To The Cinema
      
    Description:
      
        My friend John likes to go to the cinema. He can choose between system A and system B.
          
        System A : buy a ticket (15 dollars) every time
        System B : buy a card (500 dollars) and every time
        buy a ticket the price of which is 0.90 times the price he paid for the previous one.
           
        #Example: If John goes to the cinema 3 times:
        System A : 15 * 3 = 45
        System B : 500 + 15 * 0.90 + (15 * 0.90) * 0.90 + (15 * 0.90 * 0.90) * 0.90 ( = 536.5849999999999, no rounding for each ticket)
           
        John wants to know how many times he must go to the cinema so that the final result of System B, when rounded up to the next dollar, will be cheaper than System A.
          
        The function movie has 3 parameters: card (price of the card), ticket (normal price of a ticket), perc (fraction of what he paid for the previous ticket) and returns the first n such that
             
    Test task:
    
        ./vendor/bin/phpunit tests/GoingToTheCinemaTest.php
                          
- #### Kebabize
      
    Description:
       
        Modify the kebabize function so that it converts a camel case string into a kebab case.
        kebabize('camelsHaveThreeHumps') // camels-have-three-humps
        kebabize('camelsHave3Humps') // camels-have-humps
           
    Test task:
     
        ./vendor/bin/phpunit tests/KebabizeTest.php  
          
- #### Narcissistic Number
   
    Description:
       
        A Narcissistic Number is a number which is the sum of its own digits, each raised to the power of the number of digits.

    Test task:
     
        ./vendor/bin/phpunit tests/NarcissisticNumberTest.php  
          
- #### Nesting
 
    Description:
    
        Nesting
           
    Test task:
     
        ./vendor/bin/phpunit tests/NestingTest.php
                                                       
- #### Passing Cars
        
    Description:
        
        PassingCars
            
    Test task:
      
        ./vendor/bin/phpunit tests/PassingCarsTest.php
                                                                 
- #### Positive Integer
      
    Description:
        
        Positive Integer
            
    Test task:
      
        ./vendor/bin/phpunit tests/PositiveIntegerTest.php
            
- #### Rectangle Into Squares
   
    Description:
               
        Rectang leInto Squares
                   
    Test task:
             
        ./vendor/bin/phpunit tests/RectangleIntoSquaresTest.php
                       
- #### Sales Man Travel
   
    Description:
               
        Sales Man Travel
                   
    Test task:
             
        ./vendor/bin/phpunit tests/SalesManTravelTest.php
       
- #### Shortest Word
   
    Description:
               
        Simple, given a string of words, return the length of the shortest word(s).
                   
        String will never be empty and you do not need to account for different data types.
                   
    Test task:
             
        ./vendor/bin/phpunit tests/ShortestWordTest.php                                     