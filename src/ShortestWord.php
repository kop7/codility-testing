<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class ShortestWord
{

  public function shortest($word)
  {
        return min(array_map('strlen', explode(' ',$word)));
  }
}