<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class GoingToTheCinema
{

  public function cinema($array)
  {
       $card = $array[0];
       $ticket = $array[1];
       $perc = $array[2];

      $need = 0;
      do {
          $need++;
          $card += $ticket * ($perc ** $need);

      } while (ceil($card) >= ($ticket * $need));
      return $need;


  }
}