<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class ReplaceWithAlphabetPosition
{

  public function alphabetPosition($string)
  {
      $sentence = str_split($string);
      $sen = '';

      $UPPERCASE_LETTERS = range(chr(65),chr(90));
      array_unshift($UPPERCASE_LETTERS , 'item1');

      $LOWERCASE_LETTERS = range(chr(97),chr(122));
      array_unshift($LOWERCASE_LETTERS , 'item1');


      foreach ($sentence as $key => $value){
          $ul = array_search($value,$UPPERCASE_LETTERS);
          $ll = array_search($value,$LOWERCASE_LETTERS);

          if ($ul != false){
              $sen .= ' '.$ul;
          }elseif($ll !== false){
              $sen .= ' '.$ll;
          }
      }

     return ltrim($sen,' ');




  }


    /**
     * best solution
     *
     * @param $s
     * @return string
     */
    public function alphabetPosition2($s)
    {
        $value = '';
        foreach(str_split(preg_replace('/[^a-z]/i', '', $s)) as $letter) {
            $letterToNumber = ord(strtoupper($letter)) - 64;
            $value .= " {$letterToNumber}";
        }
        return trim($value);
    }
}