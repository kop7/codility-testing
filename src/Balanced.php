<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class Balanced
{

  public function balance($str1, $str2)
  {
      $counter = [
          'strLeft' => [
              '!' => 0,
              '?' => 0,
          ],
          'strRight' => [
              '!' => 0,
              '?' => 0,
          ],
          'side' => [
              'left' => 0,
              'right' => 0,
          ]
      ];


      $counter['strLeft']['!'] = substr_count($str1[0], '!');
      $counter['strLeft']['?'] = substr_count($str1[0], '?');

      $counter['left'] = ($counter['strLeft']['!'] * 2) + ($counter['strLeft']['?'] * 3);

      $counter['strRight']['!'] += substr_count($str2[0], '!');
      $counter['strRight']['?'] += substr_count($str2[0], '?');

      $counter['right'] = ($counter['strRight']['!'] * 2) + ($counter['strRight']['?'] * 3);

      if ($counter['left'] > $counter['right']) {
          return 'Left';
      } elseif ($counter['left'] < $counter['right']) {
          return 'Right';
      } else {
          return 'Balance';
      }
  }

    function balanceBest($l, $r){
        $a = substr_count($l, "!")*2+substr_count($l, "?")*3;
        $b = substr_count($r, "!")*2+substr_count($r, "?")*3;
        if ($a == $b) return "Balance"; elseif ($a < $b) return "Right"; else return "Left";
    }
}