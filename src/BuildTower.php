<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class BuildTower
{

  public function build($n)
  {
      $pad = $n * 2 - 1;
      $x = 1;
      $arr = [];

      while ($n --> 0) {

          $arr[] = str_pad(str_repeat('*', $x), $pad, ' ', STR_PAD_BOTH);
          $x += 2;
      }
      return $arr;
  }
}