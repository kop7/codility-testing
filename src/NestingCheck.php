<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

  class NestingCheck
{

  public function nesting($string) {

    $stack = 0;

    if (empty($string)){
      return 1;
    };

    foreach (str_split($string) as $item){

      if ($item =='('){
        $stack++;
      }else{
        $stack--;
      }

    }

    if ($stack === 0){
      return 1;
    }
    return 0;

  }
}