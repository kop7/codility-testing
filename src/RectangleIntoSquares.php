<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class RectangleIntoSquares
{

  public function rectang($array)
  {
      $height = $array[0];
      $width = $array[1];

      if ($height === $width) {
          return null;
      }
      $squares = [];
      while ($width > 0) {
          if ($height > $width) {
              $squares[] = $width;
              $height -= $width;
          } else {
              $squares[] = $height;
              $width -= $height;
          }
      }
      return $squares;
  }
}