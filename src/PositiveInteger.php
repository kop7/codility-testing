<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class PositiveInteger
{

  public function posInteger($integers)
  {
      $count = count($integers);
      sort($integers);


       foreach ($integers as $key => $int){
         if ($int < 0){
           unset($integers[$key]);
         }
       }
    $integers = array_values($integers);

       if (empty($integers)){
      return 1;
       }
      for ($i=0;$i < $count; $i++){

          if (isset($integers[$i+1])) {


            if( $integers[$i+1] - $integers[$i] > 1){
              return $integers[$i] + 1;
            }
          }
          else{
            return $integers[$i] + 1;
          }

      }

  }
}