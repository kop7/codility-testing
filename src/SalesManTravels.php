<?php

/**
 * Class SalesManTravels
 */
class SalesManTravels
{

	/**
	 * @param $r
	 * @param $zipcode
	 *
	 * @return string
	 */
	public function travel($r, $zipcode)
  {
	  preg_match_all("/(\d+) ([^,]+)? {$zipcode},/", "{$r},", $matches);
	  return (empty($zipcode) || empty($matches[2])) ? "{$zipcode}:/" : "{$zipcode}:" . implode(',', $matches[2]) . '/' . implode(',', $matches[1]);
  }

}