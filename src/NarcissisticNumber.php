<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class NarcissisticNumber
{

  public function narcissistic($value)
  {
      $len = strlen($value);
      $sum = 0;

      foreach (str_split($value) as $num) {
          $sum += pow($num,$len);
      }

      return ($sum == $value);

  }
}

