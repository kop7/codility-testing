<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class GettheMiddleCharacter
{

  public function middleCharacter($string)
  {

      for ($i=0;isset($string[$i]);$i++);

      if ($i % 2 == 0){
          $i /= 2;
          return $string[$i-1].$string[$i];
      }
      $i = ceil($i / 2);

      return $string[(int)$i-1];

  }
}