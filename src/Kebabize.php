<?php
/**
 * Created by PhpStorm.
 * User: mkop
 * Date: 12.02.18.
 * Time: 08:46
 */

class Kebabize
{

  public function kebabizeThis($string)
  {
      $sentence = '';
      $sen = str_split(preg_replace('/[0-9]+/', '', $string));

      for ($i = 0 ; $i < count($sen); $i++){

          if (ctype_upper($sen[$i])){
                  $sen[$i] = '-'.strtolower($sen[$i]);
          }

          $sentence .=  $sen[$i];
      }
    return ltrim($sentence,'-');
  }

    function kebabizeBest($string) {
        return strtolower(preg_replace(['/[^a-zA-Z]/', '/([A-Z])/', '/^-/'], ['', '-$1', ''], $string));
    }
}