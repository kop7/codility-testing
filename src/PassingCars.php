<?php

class PassingCars{

    public function solve($cars){

        $numberOfPairs = 0;
        $numberOfZeros = 0;

        foreach($cars as $key => $car){
            
            if($cars[$key] == 0){
                $numberOfZeros++;
                continue;
            }

            $numberOfPairs = $numberOfPairs + $numberOfZeros;
        }

        return $numberOfPairs;

    }
}